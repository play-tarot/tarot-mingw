#!/bin/bash

yum update -y || exit 1

yum install -y \
    mingw64-gcc \
    mingw64-readline \
    mingw64-nettle \
    mingw64-libxml2 \
    mingw64-glib2 \
    mingw64-cairo \
    mingw64-gtk3 \
    mingw64-hicolor-icon-theme \
    \
    make \
    wget \
    ImageMagick \
    \
    mingw32-nsis.x86_64 \
    || exit 1

rm -rf public || exit 1

mkdir -p public || exit 1

wget https://play-tarot.frama.io/tarot-releases/tarot-latest.tar.gz || exit 1
tar xf tarot-latest.tar.gz || exit 1
rm -rf tarot-latest.tar.gz || exit 1
cd tarot-* || exit 1
mingw64-configure \
    --prefix="/" \
    --exec-prefix="/" \
    --bindir="/bin" \
    --sysconfdir="/etc" \
    --libdir="/lib" \
    --includedir="/include" \
    --datarootdir="/share" \
    --datadir="/share" \
    --infodir="/share/info" \
    --mandir="/share/man" \
    --enable-runtime-prefix='TAROT_PREFIX' \
    --with-static-program-name=yes \
    HOST_SYSROOT="/usr/x86_64-w64-mingw32/sys-root/mingw" \
    || exit 1
make -j 8 nsi || exit 1
cp tarot-setup.exe ../public/ || exit 1
make -j distclean || exit 1
cd ..
